#include <stdio.h>
#define MAX_VERTICES 20
struct node{
	int data;
};
void display(int edge[][MAX_VERTICES],int n){
	printf("  ");
	for(int i = 0; i < n; i++) printf("%d ",i+1);
	printf("\n");
	for(int i = 0; i < n; i++){
		printf("%d ", i+1);
		for(int j = 0; j < n; j++) printf("%d ",edge[i][j]);
		printf("\n");
	}
}

int main()
{
	int n;
	int edge_char[MAX_VERTICES][MAX_VERTICES];
	printf("enter no of nodes:"); scanf("%d",&n);
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			scanf(" %[1,0]%d", &edge_char[i][j]);
	display(edge_char,n);
}
