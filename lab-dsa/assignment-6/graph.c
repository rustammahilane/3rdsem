// structure of graph using stack
#include <stdio.h>
#include <stdlib.h>
#define MAX_VERTICES 10
struct node{
	int v;
	struct node *next;
}*graph[MAX_VERTICES];
typedef struct node *node_ptr;
int TOP = -1;

void insertNode (node_ptr graph[++TOP]){
		graph[TOP] = (node_ptr)malloc(sizeof(struct node));
		graph[TOP]->v = TOP;
		graph[TOP]->next = NULL;
}

void insertEdge (node_ptr vertex, int v1, int v2){
	if(v1 >= 0 && v1,v2 <= TOP){
		node_ptr new = (node_ptr)malloc(sizeof(struct node));
		new->v = v2;
		new->next = NULL;
		while(vertex->next) vertex = vertex->next;
		vertex->next = new;
	}
	else fprintf(stderr,"error at push()\nNo vertex %d", v2);
}

void display_vertices(node_ptr graph[]){
	if(!graph) fprintf(stderr,"no graph");
	else{
		node_ptr vertex = (node_ptr)malloc(sizeof(struct node));
		for(int i = 0; i <= TOP; i++){
	          	vertex = graph[i];
			while(vertex!= NULL){
				printf("%d-> ", vertex->v);
				vertex = vertex->next;
			}
			printf("\n");
		}
	}
}

int main()
{
	int n = 0;
	int choice = 0;
	do{
		printf("\n------------\n");
		printf("0.exit\n1.Insert vertex\n2.Display\n3.Insert Edge\n");
		printf("enter choice:");
		scanf("%d",&choice);
		switch(choice){
			case 0: break;
			case 1: insertNode(graph);	printf("Inserted vertex: %d", TOP);
				break;
			case 2: display_vertices(graph); break;
			case 3: printf("Insert edge (vertices with space) :"); int v1,v2; scanf("%d%d",&v1,&v2);
				insertEdge(graph[v1],v1,v2); break;
		}
	}while(choice != 0);
	return 0;
}
